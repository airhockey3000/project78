/* Arduino Tutorial: Learn how to use an LCD 16x2 screen
   More info: http://www.ardumotive.com/how-to-use-an-lcd-dislpay-en.html  */

//Include LCD library
#include <LiquidCrystal.h>
#include <SPI.h>
#include <Wire.h>

#define homeButton A5
#define awayButton A6

#define diffButton 13

//Set the pins for the LCD Display to the Arduino pins
/*const int rs = 3, en = 4, d4 = 8, d5 = 7, d6 = 6, d7 = 5;*/
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;

LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
/*
 *LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
 */

int home = 0;
int away = 0;
char rx_byte = 0;
char inputSerial;

int buttonState = 0;
int diffLevel = 2; 		//2 HARD 1 MEDIUM 0 EASY

int incomingByte = 0; // for incoming serial data

void setup() {
	pinMode(diffButton, INPUT_PULLUP);
/*
 *SCOREBOARD PART
 */
  pinMode(homeButton, INPUT);
  pinMode(awayButton, INPUT);

  Serial.begin(115200);   // Initiate a serial communication
  /*
   *SPI.begin();      // Initiate  SPI bus
   *mfrc522.PCD_Init();   // Initiate MFRC522
   */

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.setCursor(2,0);
  lcd.print("Home -- Away");
  lcd.setCursor(2,1);
  lcd.print("Game  Scores");
/*
 *END SCOREBOARD PART
 */

}

void loop() {
  buttonState = digitalRead(diffButton);
  if (Serial.available()>0) {    // is a character available?
    //rx_byte = Serial.read();
    inputSerial = Serial.read();
    mainfunction(inputSerial);

    // read the incoming byte:
/*
 *    incomingByte = Serial.read();
 *
 *    // say what you got:
 *    Serial.print("I received: ");
 *    Serial.println(incomingByte, DEC);
 */
  }else{
  }
  if (buttonState == HIGH) {
    difffunc();
  }
}

/*
 *SCOREBOARD FUNCTION
 */
void mainfunction(char input){
	//if(digitalRead(homeButton)==HIGH){
	//else if(digitalRead(awayButton)==HIGH){
	switch (input) {
	/* Reset */
	case 'r':
		home=0;
		away=0;
		resetScreen(2,0);
		lcd.print("Home -- Away");
		lcd.setCursor(3,1);
		lcd.print(home);
		lcd.setCursor(12,1);
		lcd.print(away);
		break;
	/* Initialize */
	case 'i':
		for(int x = 0; x < 2; x++) {
			resetScreen(0,0);
			lcd.print("Initialising.");
			delay(500);
			resetScreen(0,0);
			lcd.print("Initialising..");
			delay(500);
			resetScreen(0,0);
			lcd.print("Initialising...");
			delay(500);
		}	

		resetScreen(0,0);
		lcd.print("Check if all dem-");
		lcd.setCursor(0,1);
		lcd.print("ands are correct");
		delay(500);
		break;
	/* Prediction mode */
	case 'd':
		resetScreen(1,0);
		lcd.print("Ready to play!");
		delay(500);
		break;
	/* Goal for home */
	case 'h':
		home++;
		resetScreen(2,0);
		lcd.print("Home -- Away");
		lcd.setCursor(3,1);
		lcd.print(home);
		lcd.setCursor(12,1);
		lcd.print(away);
		break;
	/* Goal for away */
	case 'a':
		away++;
		resetScreen(2,0);
		lcd.print("Home -- Away");
		lcd.setCursor(3,1);
		lcd.print(home);
		lcd.setCursor(12,1);
		lcd.print(away);
		break;
	default:
		resetScreen(0, 0);
		lcd.print("SOMETHING WENT WRONG");
		lcd.setCursor(0,1);
		lcd.print(input);
		Serial.print(input);
		break;
	}
}

void resetScreen(int y, int x){
  lcd.clear(); // this is to clear the screen of the previous displayed text
  lcd.setCursor(y, x); // set the cursor to column 0, line 0 or 1
                      // (note: line 1 is the second row, since counting begins with 0):
}

void displayUID(String x){
  lcd.setCursor(0,1);
  lcd.print(x);
}
/*
 *END SCOREBOARD FUNCTIONS
 */
void difffunc() {
    Serial.println("kaas");
    diffLevel--;
    delay(1000);
    if(diffLevel < 0) {
    	diffLevel = 3;
    }
    if(diffLevel == 3) {
    	resetScreen(2,0);
    	lcd.print("Difficulty");
    	lcd.setCursor(3,1);
    	lcd.print("D Y N A M I C");
    	Serial.print("dynamic");
    }else if(diffLevel == 2) {
    	resetScreen(2,0);
    	lcd.print("Difficulty");
    	lcd.setCursor(3,1);
    	lcd.print("H A R D");
    	Serial.print("hard");
    } else if(diffLevel == 1) {
    	resetScreen(2,0);
    	lcd.print("Difficulty");
    	lcd.setCursor(3,1);
    	lcd.print("M E D I U M");
    	Serial.print("medium");
    } else if(diffLevel == 0) {
    	resetScreen(2,0);
    	lcd.print("Difficulty");
    	lcd.setCursor(3,1);
    	lcd.print("E A S Y");
    	Serial.print("easy");
    }
}
