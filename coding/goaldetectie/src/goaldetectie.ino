const byte ledPin = 13;
const byte interruptPin = 2;
volatile byte detected = LOW; //wordt in ram bijgehouden

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), blink, FALLING);
}

void loop() {
  if (detected == HIGH){
    digitalWrite(ledPin, HIGH);
    delay(1000);
    digitalWrite(ledPin, LOW);
    detected = LOW;
  }  
}

void blink() {
  digitalWrite(ledPin, HIGH);
  detected = HIGH;
}
