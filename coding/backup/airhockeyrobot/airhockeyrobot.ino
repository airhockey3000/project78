#include <LiquidCrystal.h>
#include <SPI.h>
#include <Wire.h>
#include <AccelStepper.h>

//#include "qtest.h"

#define DEBUG 1

#define homeButton A5
#define awayButton A6

//Define stepper motor connections
#define X_STEP_PIN      54
#define X_DIR_PIN       55
#define X_ENABLE_PIN    38
#define X_MIN_PIN       3 //endstop
#define X_MAX_PIN       2 //endstop
#define dirPin          X_DIR_PIN
#define stepPin         X_STEP_PIN
#define enableX         X_ENABLE_PIN
#define homeEndPin      X_MIN_PIN
#define maxEndPin       X_MAX_PIN


#define maxSpeed 5000
#define acceleration 30000
#define homeSpeed 1500

#define left 13
#define right 12

boolean maxPosSet = false;

int maxPosition = 1300;

char serialCommand;


//Create stepper object
AccelStepper stepper(1, stepPin, dirPin); //motor interface type must be set to 1 when using a driver.

void home() {
        stepper.setMaxSpeed(homeSpeed);
        if (DEBUG) {
                Serial.println("---Homing...---");
        }
        while (digitalRead(homeEndPin) != LOW) {
                if (DEBUG) {
                        //Serial.println("Home not end triggered");
                }
                stepper.move(-10);
                stepper.run();
        }
        stepper.setCurrentPosition(0);
        stepper.runToNewPosition(10);

        if (DEBUG) {
                Serial.println("Position set to zero");
                Serial.print("Current position is:");
                Serial.println(stepper.currentPosition());
        }

        stepper.setMaxSpeed(maxSpeed);
        serialCommand = '0';
}

void setMaxPosition() {
        if (DEBUG) {
                Serial.println("---Calculating max position---");
        }
        home();
        delay(500);
        stepper.setMaxSpeed((int)(maxSpeed / 3));

        while (digitalRead(maxEndPin) != LOW) {
                if (DEBUG) {
                        //Serial.println("Max not end triggered");
                }
                stepper.move(10);
                stepper.run();
        }
        if (DEBUG) {
                Serial.println("Max end triggered");
        }
        maxPosition = stepper.currentPosition();
        stepper.runToNewPosition(maxPosition - 10);

        if (DEBUG) {
                Serial.print("New max position: ");
                Serial.println(maxPosition);
        }
        maxPosSet = true;
        serialCommand = '0';

        stepper.setMaxSpeed(maxSpeed);
}

void moveTo(int position) {
        if (position > maxPosition) {
                if(DEBUG){
                        Serial.println("Cannot exceed max position");
                }
                position = maxPosition;
        }
        if (position < 0) {
                position = 0;
                if(DEBUG){
                        Serial.println("Cannot exceed min position");
                }
        }
        if (DEBUG) {
                Serial.print("moving to ");
                Serial.println(position);
        }
        stepper.moveTo(position);
}

void move(int relativePosition) {
        int newPosition = (stepper.currentPosition() + relativePosition);

        moveTo(newPosition);
}

void runStepperPos() {
        while(stepper.currentPosition() != stepper.targetPosition()){

                //stepper.runToPosition();
                stepper.run();
        }
        if (DEBUG) {
                if (stepper.currentPosition() == stepper.targetPosition()) {
                        Serial.print("Moved to ");
                        Serial.println(stepper.currentPosition());
                }
        }
}



void setup()
{
        Serial.begin(115200);
        stepper.setMaxSpeed(maxSpeed); //maximum steps per second
        stepper.setAcceleration(acceleration);
        pinMode(homeEndPin, INPUT_PULLUP);
        pinMode(maxEndPin, INPUT_PULLUP);
        pinMode(left, INPUT_PULLUP);
        pinMode(right, INPUT_PULLUP);
        pinMode(X_ENABLE_PIN, OUTPUT);
        digitalWrite(X_ENABLE_PIN, LOW);
        if (DEBUG) {
                Serial.println("Setup complete \n");
                //Serial.println(qtest.returnInt());
        }
        Serial.println("h: Home\nm: set min/max position\nc: center\nd: demo\ni: interactive demo\n\nEnter command:");
}

void loop()
{

        if (Serial.available()) {
                serialCommand = Serial.read();
        }
        switch(serialCommand){
                case 'h': //home
                home();
                serialCommand = '0';
                break;

                case 'm': //max (set max pos)
                setMaxPosition();
                serialCommand = '0';
                break;

                case 'c': //center
                moveTo( (int)(maxPosition/2) );
                stepper.runToPosition();
                serialCommand = '0';
                break;

                case 'd': // demo
                if (DEBUG) {
                        if (maxPosSet) {
                                moveTo(0 +10);
                                stepper.runToPosition();
                                moveTo(maxPosition - 10);
                                stepper.runToPosition();
                                moveTo(0 + 10);
                                stepper.runToPosition();
                        }
                }

                if (!maxPosSet) {
                        moveTo(400);
                        stepper.runToPosition();
                        delay(100);
                        moveTo(0);
                        stepper.runToPosition();

                        delay(1000);

                        moveTo(-400);
                        //TODO: remove below
                        stepper.runToPosition();
                        moveTo(0);
                        //TODO: remove below
                        stepper.runToPosition();
                }
                break;

                case 'i': //interactive demo
                boolean run = true;
                while (run){
                        if (Serial.available()) {
                                if(Serial.read() == "x"){
                                        run = false;
                                }
                        }
                        runStepperPos();
                        if(!maxPosSet){
                                Serial.println("Run min/max first");
                                serialCommand = '0';
                                break;
                        }
                        if (!digitalRead(left)){

                                move(12);
                        }
                        else if (!digitalRead(right)){
                                move(-12);
                        }
                        else{}

                }
                Serial.println("End of interactive demo");
                serialCommand = '0';
                break;

        }
}
