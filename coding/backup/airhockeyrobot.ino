/*
 * Copyright 2019, Jeffrey Stoel, 
 *	  	   Quentin Hoogwerf, 
 *		   Paul Wondel, 
 *		   Paul de Hek, 
 *		   AirHockey3000, All rights reserved.
 */

/* Including libraries */
#include <LiquidCrystal.h>
#include <SPI.h>
#include <Wire.h>
#include <AccelStepper.h>

/* General defines */
#define DEBUG 			0

/* Stepper motor pins and defs */
#define dirPin 			22
#define stepPin	 		23
#define homeEndPin 		10
#define maxEndPin 		9

#define MAX_SPEED 		1500		//Maximum speed the stepper motor runs.
#define ACCELERATION 		30000	
#define HOME_SPEED 		100		//The speed when homing. Lower cause more accurate.

/* Endstop pins */
#define leftEndStop 		51
#define rightEndStop	 	52

/* Buttons */
#define diffButton 		13		//Button for setting difficulty of the robot.

/* Difficulties of the robot */
#define DIFF_EASY 		0.8
#define DIFF_MEDIUM 		0.9
#define DIFF_HARD 		1
#define DIFF_DYNAMIC 		1

/*
 * Defs for the serial protocol, when the incoming char is a [ we know it's a
 * coordinate, therefore we use a char array. So we can get the full coordinate.
 */
const byte numChars = 		32;
char receivedChars[numChars];
boolean recvCoordinates = 	false;
boolean newData = 		false;
char serialCommand;
String yCoordinates;

/* Max position of the stepper, predefined as 1300 */
boolean maxPosSet = 		false;
int MAX_POS = 			1300;

/* Scoreboard pins and defs */
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
int homeG = 			0;
int awayG = 			0;

/* States */
int buttonState =	 	0;

/* Current difficulty */
int diffLevel = 		2;              //2 HARD 1 MEDIUM 0 EASY 3 DYNAMIC

/* Create stepper object */
AccelStepper stepper(1, stepPin, dirPin); //motor interface type must be set to 1 when using a driver.



/* Home function, sets the stepper to the home position */
void home() 
{
        stepper.setMaxSpeed(HOME_SPEED);
        if (DEBUG) {
                Serial.println("---Homing...---");
        }
        while (digitalRead(homeEndPin) != LOW) {
                if (DEBUG) {
                        //Serial.println("Home not end triggered");
                }
                stepper.move(-10);
                stepper.run();
        }
        stepper.setCurrentPosition(0);
        stepper.runToNewPosition(10);
        if (DEBUG) {
                Serial.println("Position set to zero");
                Serial.print("Current position is:");
                Serial.println(stepper.currentPosition());
        }
        stepper.setMaxSpeed(MAX_SPEED);
        serialCommand = '0';
}

/* Function that calculates max steps stepper can make */
void setMaxPosition() 
{
        if (DEBUG) {
                Serial.println("---Calculating max position---");
        }
        home();
        delay(500);
        stepper.setMaxSpeed((int)(MAX_SPEED / 3));
        while (digitalRead(maxEndPin) != LOW) {
                if (DEBUG) {
                        //Serial.println("Max not end triggered");
                }
                stepper.move(10);
                stepper.run();
        }
        if (DEBUG) {
                Serial.println("Max end triggered");
        }
        MAX_POS = stepper.currentPosition();
        stepper.runToNewPosition(MAX_POS - 10);
        if (DEBUG) {
                Serial.print("New max position: ");
                Serial.println(MAX_POS);
        }
        maxPosSet = true;
        serialCommand = '0';
        stepper.setMaxSpeed(MAX_SPEED);
}

/* Function that moves the stepper to a certain position */
void moveTo(int position) {
        if (position > MAX_POS) {
                if(DEBUG){
                        Serial.println("Cannot exceed max position");
                }
                position = MAX_POS;
        }
        if (position < 0) {
                position = 0;
                if(DEBUG){
                        Serial.println("Cannot exceed min position");
                }
        }
        if (DEBUG) {
                Serial.print("moving to ");
                Serial.println(position);
        }
        stepper.moveTo(position);
}

/* What the actual fuck, ask Q */
void move(int relativePosition) {
        int newPosition = (stepper.currentPosition() + relativePosition);
        moveTo(newPosition);
}

/* What the actual fuck, ask Q */
void runStepperPos() {
        while(stepper.currentPosition() != stepper.targetPosition()){
                //stepper.runToPosition();
                stepper.run();
        }
        if (DEBUG) {
                if (stepper.currentPosition() == stepper.targetPosition()) {
                        Serial.print("Moved to ");
                        Serial.println(stepper.currentPosition());
                }
        }
}

/* lcdSetup function, home screen kind of look */
void lcdSetup() {
	// set up the LCD's number of columns and rows:
	lcd.begin(16, 2);
	// Print a message to the LCD.
	lcd.setCursor(2,0);
	lcd.print("Home -- Away");
	lcd.setCursor(2,1);
	lcd.print("Game  Scores");
}

/* Case switch for serial input */
void serialFunction(char input){
        //if(digitalRead(homeButton)==HIGH){
        //else if(digitalRead(awayButton)==HIGH){
        switch (input) {
        /* Reset */
        case 'r':
                homeG=0;
                awayG=0;
                resetScreen(2,0);
                lcd.print("Home -- Away");
                lcd.setCursor(3,1);
                lcd.print(homeG);
                lcd.setCursor(12,1);
                lcd.print(awayG);
                break;
        /* Initialize */
        case 'i':
                resetScreen(0,0);
                lcd.print("Initialising.");
		setMaxPosition();
                resetScreen(0,0);
                lcd.print("Initialising..");
                delay(500);
                resetScreen(0,0);
		moveTo((int)(MAX_POS/2));
		stepper.runToPosition();
                serialCommand = '0';
                lcd.print("Initialising...");
		String maxPos = String(MAX_POS);
        	Serial.print("d"+maxPos);
                delay(500);
                resetScreen(0,0);
                lcd.print("Press space if all");
                lcd.setCursor(0,1);
                lcd.print("demands are correct");
                delay(500);
                break;
        /* Prediction mode */
        case 'p':
                resetScreen(1,0);
                lcd.print("You're never scoring!");
                delay(500);

                break;
        /* Goal for home */
        case 'H':
                homeG++;
                resetScreen(2,0);
                lcd.print("Home -- Away");
                lcd.setCursor(3,1);
                lcd.print(homeG);
                lcd.setCursor(12,1);
                lcd.print(awayG);
                break;
        /* Goal for away */
        case 'A':
                awayG++;
                resetScreen(2,0);
                lcd.print("Home -- Away");
                lcd.setCursor(3,1);
                lcd.print(homeG);
                lcd.setCursor(12,1);
                lcd.print(awayG);
                break;
        //test, mag straks weggehaald worden
        case '5':
                setMaxPosition();
                Serial.println("Test complete!");
                break;
        //einde test
        default:
                resetScreen(0, 0);
                lcd.print("SOMETHING WENT WRONG");
                lcd.setCursor(0,1);
                lcd.print(input);
                Serial.print(input);
                break;
        }
}

/* Resets the screen */
void resetScreen(int y, int x){
  lcd.clear(); // this is to clear the screen of the previous displayed text
  lcd.setCursor(y, x); // set the cursor to column 0, line 0 or 1
                      // (note: line 1 is the second row, since counting begins with 0):
}

/* Difficulty changing function */
void difffunc() {
    diffLevel--;
    delay(100);
    if(diffLevel < 0) {
        diffLevel = 3;
    }
    if(diffLevel == 3) {
        resetScreen(2,0);
        lcd.print("Difficulty");
        lcd.setCursor(3,1);
        lcd.print("D Y N A M I C");
        Serial.print("dynamic");
    }else if(diffLevel == 2) {
        resetScreen(2,0);
        lcd.print("Difficulty");
        lcd.setCursor(3,1);
        lcd.print("H A R D");
        Serial.print("hard");
    } else if(diffLevel == 1) {
        resetScreen(2,0);
        lcd.print("Difficulty");
        lcd.setCursor(3,1);
        lcd.print("M E D I U M");
       Serial.print("medium");
    } else if(diffLevel == 0) {
        resetScreen(2,0);
        lcd.print("Difficulty");
        lcd.setCursor(3,1);
        lcd.print("E A S Y");
        Serial.print("easy");
    }
}

/* Quentins function, ask Q  */
void quentinRobot() {
        switch(serialCommand){
                case 'h': //home
                home();
                serialCommand = '0';
                break;

                case 'm': //max (set max pos)
                setMaxPosition();
                serialCommand = '0';
                break;

                case 'c': //center
                moveTo( (int)(MAX_POS/2) );
                stepper.runToPosition();
                serialCommand = '0';
                break;

                case 'd': // demo
                if (DEBUG) {
                        if (maxPosSet) {
                                moveTo(0 +10);
                                stepper.runToPosition();
                                moveTo(MAX_POS - 10);
                                stepper.runToPosition();
                                moveTo(0 + 10);
                                stepper.runToPosition();
                        }
                }

                if (!maxPosSet) {
                        moveTo(400);
                        stepper.runToPosition();
                        delay(100);
                        moveTo(0);
                        stepper.runToPosition();

                        delay(1000);

                        moveTo(-400);
                        //TODO: remove below
                        stepper.runToPosition();
                        moveTo(0);
                        //TODO: remove below
                        stepper.runToPosition();
                }
                break;

                case 'i': //interactive demo
                boolean run = true;
                while (run){
                        if (Serial.available()) {
                                if(Serial.read() == "x"){
                                        run = false;
                                }
                        }
                        runStepperPos();
                        if(!maxPosSet){
                                Serial.println("Run min/max first");
                                serialCommand = '0';
                                break;
                        }
                        if (!digitalRead(leftEndStop)){

                                move(12);
                        }
                        else if (!digitalRead(rightEndStop)){
                                move(-12);
                        }
                        else{}

                }
                Serial.println("End of interactive demo");
                serialCommand = '0';
                break;

        }
}

/* Function when receiving a coordinate */
void recvString() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '[';
    char endMarker = ']';
    char rc;

    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();
	stepper.run();

        if (recvInProgress == true) {
	    stepper.run();
            if (rc != endMarker) {
		stepper.run();
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
		    stepper.run();
                    ndx = numChars - 1;
                }
            }
            else {
		stepper.run();
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
		showNewData();
            }
        }

        else if (rc == startMarker) {
	    stepper.run();
            recvInProgress = true;
        }
    }
}

/* Function to show the data received */
void showNewData() {
    if (newData == true) {
        /* Serial.print("This just in ... "); */
	String str(receivedChars);
        /* Serial.println(str.toInt()); */
        newData = false;
	stepper.run();
	moveTo(str.toInt());
    }
}

/* Setup function */
void setup()
{
        Serial.begin(115200);

	/* Scoreboard stuff */
	pinMode(diffButton, INPUT_PULLUP);
	lcdSetup();

	/* Stepper setup */
        stepper.setMaxSpeed(MAX_SPEED); //maximum steps per second
        stepper.setAcceleration(ACCELERATION);
        pinMode(homeEndPin, INPUT_PULLUP);
        pinMode(maxEndPin, INPUT_PULLUP);
        pinMode(leftEndStop, INPUT_PULLUP);
        pinMode(rightEndStop, INPUT_PULLUP);

        if (DEBUG) {
                Serial.println("Setup complete \n");
        }
	/* Serial.println("h: Home\nm: set min/max position\nc: center\nd: demo\ni: interactive demo\n\nEnter command:"); */
}

/* Loop function */
void loop()
{
	buttonState = digitalRead(diffButton);
	/* Read Serial, if the serial is d then we are receiving coordinates, else see switch case */
        if (Serial.available()) {
                serialCommand = Serial.read();
		if(serialCommand != 'd') {
			recvCoordinates = false;
			serialFunction(serialCommand);
		} else {
			recvCoordinates = true;
			while(recvCoordinates == true) {
				stepper.run();
				recvString();
			}
		}
        }
}
