#include <iostream>
#include <stdlib.h>
#include <thread>

using namespace std;

void playSound(int songNr)
{
	cout << songNr << endl;
	char statement[20];
	cout << "made char array - ";
	sprintf(statement, "./playAudio.sh %d", songNr);
	cout << "statement = " << statement << endl;
	system(statement);
}

int main(){
	cout << "enter the ID of the sound you want to play" << endl;
	int songNr;
	cin >> songNr;
	playSound(songNr);
	return 0;
}
