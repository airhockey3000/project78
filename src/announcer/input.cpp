#include <wiringPi.h>
#include "playAudio.h"
#include <iostream>
#define PLAYERONE_SENSOR 0
#define PLAYERTWO_SENSOR 1

using namespace std;

const int maxScore = 5;

int pOneScore = 0;
int pTwoScore = 0;

void announceScores()
{
	cout << pOneScore << "-" << pTwoScore << endl;
	PlayAudio audio;
	audio.play(pOneScore);
	audio.play(pTwoScore);
}
void announceWinner(int winner)
{
	PlayAudio audio;
	if(winner == 1)
	{
		audio.play(-1);
	} else {
		audio.play(-2);
	}
}

void resetGame()
{
	pOneScore = 0;
	pTwoScore = 0;
}

void addScorePlayerOne(void)
{
	cout << "Player 1 SCORED" << endl;
	pOneScore++;
	announceScores();
	if(pOneScore >= maxScore)
	{
		announceWinner(1);
		resetGame();
	}
}

void addScorePlayerTwo(void)
{
	cout << "Player 2 SCORED" << endl;
	pTwoScore++;
	announceScores();
	if(pTwoScore >= maxScore)
	{
		announceWinner(2);
		resetGame();
	}
}
int main(){
	int scoreCounter = 0;
	wiringPiSetup();
	pinMode(PLAYERONE_SENSOR, INPUT);
	pinMode(PLAYERTWO_SENSOR, INPUT);

	wiringPiISR(PLAYERONE_SENSOR,INT_EDGE_RISING, &addScorePlayerOne);
	wiringPiISR(PLAYERTWO_SENSOR,INT_EDGE_RISING, &addScorePlayerTwo);
	while (true){
		//endless loop where the rest of the program would be executed
	}
	return 0;
}
