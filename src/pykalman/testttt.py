#! /usr/bin/env python2.7

import serial
import cv2 as cv
import numpy as np
import sys

def nothing(x):
    # any operation
    pass

# cam = cv.VideoCapture('/home/jeffrey/tmp/met_stift.mov')
# cam = cv.VideoCapture(0)
cv.namedWindow("Trackbars")
cv.createTrackbar("L-H", "Trackbars", 0, 180, nothing)
cv.createTrackbar("L-S", "Trackbars", 0, 255, nothing)
cv.createTrackbar("L-V", "Trackbars", 131, 255, nothing)
cv.createTrackbar("U-H", "Trackbars", 180, 180, nothing)
cv.createTrackbar("U-S", "Trackbars", 255, 255, nothing)
cv.createTrackbar("U-V", "Trackbars", 243, 255, nothing)

font = cv.FONT_HERSHEY_COMPLEX

while True:
    #  cam = cv.imread('/home/jeffrey/tmp/tafelfoto.jpeg')
    cam = cv.imread('opencv_frame_1.png')
    # while True:
    # ret, frame = cam.read()
    # cv.imshow("test", frame)
    # if not ret:
        # break
    hsv = cv.cvtColor(cam, cv.COLOR_BGR2HSV)
    
    l_h = cv.getTrackbarPos("L-H", "Trackbars")
    l_s = cv.getTrackbarPos("L-S", "Trackbars")
    l_v = cv.getTrackbarPos("L-V", "Trackbars")
    u_h = cv.getTrackbarPos("U-H", "Trackbars")
    u_s = cv.getTrackbarPos("U-S", "Trackbars")
    u_v = cv.getTrackbarPos("U-V", "Trackbars")
    
    lower_red = np.array([l_h, l_s, l_v])
    upper_red = np.array([u_h, u_s, u_v])
    
    mask = cv.inRange(hsv, lower_red, upper_red)
    kernel = np.ones((5, 5), np.uint8)
    mask = cv.erode(mask, kernel)
    
    # Contours detection
    contours, hierarchy = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    
    ylist = []
    
    for cnt in contours:
        area = cv.contourArea(cnt)
        approx = cv.approxPolyDP(cnt, 0.02*cv.arcLength(cnt, True), True)
        x = approx.ravel()[0]
        y = approx.ravel()[1]
        ylist.append(y)
    
        if area > 400:
            print(approx)
            cv.drawContours(cam, [approx], 0, (0, 0, 0), 5)
    
            # if len(approx) == 3:
                # cv.putText(frame, "Triangle", (x, y), font, 1, (0, 0, 0))
            if len(approx) == 4:
                cv.putText(cam, "Rectangle", (x, y), font, 1, (0, 0, 0))
            # elif 10 < len(approx) < 20:
                # cv.putText(frame, "Circle", (x, y), font, 1, (0, 0, 0))
    
    
    cv.imshow("test", cam)
    cv.imshow("mask", mask)
    key = cv.waitKey(1)
    if key == 27:
        break


# cam.release()
# cv.destroyAllWindows()
