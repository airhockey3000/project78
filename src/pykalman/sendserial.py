#! /usr/bin/env python2.7
#----------------------------------------------------------------------------#
#  Imports
import serial
import cv2 as cv
import numpy as np
import sys
import os
import time
#----------------------------------------------------------------------------#
#Setup serial
#ttyUSB0 is configured for a linux machinser = serial.Serial('/dev/ttyUSB0',9600, timeout=0.001) 
ser = serial.Serial('/dev/ttyACM0',115200, timeout=0.001) 
#----------------------------------------------------------------------------#
#Performs required image processing to get puck coordinated in the video
#Main Function
def main():
    for x in range(1000,2000,20):    
        goToCoord = str(x)
        ser.write("d[")
        ser.write(goToCoord)
        ser.write("]")
        print(goToCoord)    

if __name__ == "__main__":
    main()

print('Program Completed!')
