#! /usr/bin/env python2.7

#--------------------------------------------------------------------
# Implements Puck motion prediction using Kalman Filter
#
# Author: Sriram Emarose [sriram.emarose@gmail.com]
#
        # lowerBound = np.array([100,200,90], dtype = "uint8")
        # upperBound = np.array([170,255,190], dtype = "uint8")
#
#
#--------------------------------------------------------------------

import serial
import cv2 as cv
import numpy as np
import sys

#----------------------------------------------------------------------------#

# Setup serial
# ttyUSB0 is configured for a linux machine
# ser = serial.Serial('//ttyUSB0',9600, timeout=0.001)

#----------------------------------------------------------------------------#


class KalmanFilter:

    kf = cv.KalmanFilter(4, 2)
    kf.measurementMatrix = np.array([[1, 0, 0, 0], [0, 1, 0, 0]], np.float32)
    kf.transitionMatrix = np.array([[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]], np.float32)

    def Estimate(self, coordX, coordY):
        ''' This function estimates the position of the object'''
        measured = np.array([[np.float32(coordX)], [np.float32(coordY)]])
        self.kf.correct(measured)
        predicted = self.kf.predict()
        return predicted



#Performs required image processing to get puck coordinated in the video
class ProcessImage:

    def DetectObject(self):
        DIFF_HARD = 1
        DIFF_MEDIUM = 0.9
        DIFF_EASY = -1
        actualDifficulty = DIFF_HARD

        # 1280x720
        vid = cv.VideoCapture('/home/jeffrey/tmp/lol.mov')

        # vid = cv.VideoCapture('out.mp4')

        if(vid.isOpened() == False):
            print('Cannot open input video')
            return

        width = int(vid.get(3))
        height = int(vid.get(4))
        print(width)
        print(height)

        # Create Kalman Filter Object
        kfObj = KalmanFilter()
        predictedCoords = np.zeros((2, 1), np.float32)

        while(vid.isOpened()):
            rc, frame = vid.read()
            # raw = ser.readline()
            # serial_data = raw.strip().decode("utf-8")
            
            # if serial_data == "kaas":
                # print("ik ontvang u")
            # if serial_data == "hard":
                # actualDifficulty = DIFF_HARD
                # print("HARD")
            # elif serial_data == "medium":
                # print("MEDIUM")
                # actualDifficulty = DIFF_MEDIUM
            # elif serial_data == "easy":
                # print("EASY")
                # actualDifficulty = DIFF_EASY

            if(rc == True):
                [puckX, puckY] = self.DetectPuck(frame)
                predictedCoords = kfObj.Estimate(puckX, puckY)

                # Draw Actual coords from segmentation
                cv.circle(frame, (int(puckX), int(puckY)), 10, [0,0,255], 2, 8)
                # cv.line(frame,(0, 170), (width, 170), [255, 0, 0], 2, 8)
                # cv.line(frame,(0, 20), (width, 20), [255, 0, 0], 2, 8)
                cv.line(frame,(0, 300), (width, 300), [255, 0, 0], 2, 8)
                cv.line(frame,(0, 0), (width, 0), [255, 0, 0], 2, 8)
                
                # if((predictedCoords[0] - puckX) > 0 and (predictedCoords[1] - puckY) > 0):
                    # extraX = 2;
                    # extraY = 2;
                # if((predictedCoords[0] - puckX) < 0 and (predictedCoords[1] - puckY) < 0):
                    # extraX = 0.5;
                    # extraY = 0.5;
                # if((predictedCoords[0] - puckX) > 0 and (predictedCoords[1] - puckY) < 0):
                    # extraX = 2;
                    # extraY = 0.5;
                # if((predictedCoords[0] - puckX) < 0 and (predictedCoords[1] - puckY) > 0):
                    # extraX = 0.5;
                    # extraY = 2;
                # else:
                    # extra = 1;
                xn = puckX
                yn = puckY
                xn1 = (predictedCoords[0])
                yn1 = (predictedCoords[1])
                dx = predictedCoords[0] - puckX
                dy = predictedCoords[1] - puckY

                
                for x in range(0, 60): 
                    cv.line(frame,(int(xn), int(yn)), (xn1, yn1), [0, 0, 0], 2, 8)
                    xn = xn1
                    yn = yn1
                    xn1 = xn1 + dx
                    yn1 = yn1 + dy
                    if(yn1 < 300 and yn1 > 0):
                        dy = dy
                    if(yn1 >= 300 or yn1 <= 0):
                        dy = -dy
                    if(xn1 >= 1157 and xn1 <= 1210):
                        print(yn1)

                cv.imshow('Input',frame)

                if (cv.waitKey(20) & 0xFF == ord('w')):
                    break

            else:
                break

        vid.release()
        cv.destroyAllWindows()

    # Segment the green puck in a given frame
    def DetectPuck(self, frame):
        # Set threshold to filter only green color & Filter it
        lowerBound = np.array([80,160,40], dtype = "uint8")
        upperBound = np.array([150,255,140], dtype = "uint8")
        greenMask = cv.inRange(frame, lowerBound, upperBound)

        # Dilate
        kernel = np.ones((5, 5), np.uint8)
        greenMaskDilated = cv.dilate(greenMask, kernel)
        #cv.imshow('Thresholded', greenMaskDilated)

        # Find puck blob as it is the biggest green object in the frame
        [nLabels, labels, stats, centroids] = cv.connectedComponentsWithStats(greenMaskDilated, 8, cv.CV_32S)

        # First biggest contour is image border always, Remove it
        stats = np.delete(stats, (0), axis = 0)
        try:
            maxBlobIdx_i, maxBlobIdx_j = np.unravel_index(stats.argmax(), stats.shape)

        # This is our puck coords that needs to be tracked
            puckX = stats[maxBlobIdx_i, 0] + (stats[maxBlobIdx_i, 2]/2)
            puckY = stats[maxBlobIdx_i, 1] + (stats[maxBlobIdx_i, 3]/2)
            return [puckX, puckY]
        except:
               pass

        return [0,0]


#Main Function
def main():

    processImg = ProcessImage()
    processImg.DetectObject()
    

if __name__ == "__main__":
    main()

print('Program Completed!')

