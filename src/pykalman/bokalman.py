#! /usr/bin/env python2.7
#----------------------------------------------------------------------------#
#  Imports
import serial
import cv2 as cv
import numpy as np
import sys
import os
import time
#----------------------------------------------------------------------------#
#Setup serial
#ttyUSB0 is configured for a linux machinser = serial.Serial('/dev/ttyUSB0',9600, timeout=0.001) 
ser = serial.Serial('/dev/ttyACM0',115200, timeout=0.001) 
#----------------------------------------------------------------------------#
#KalmanFilter, for predicting
class KalmanFilter:
    kf = cv.KalmanFilter(4, 2)
    kf.measurementMatrix = np.array([[1, 0, 0, 0], [0, 1, 0, 0]], np.float32)
    kf.transitionMatrix = np.array([[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]], np.float32)
    def Estimate(self, coordX, coordY):
        ''' This function estimates the position of the object'''
        measured = np.array([[np.float32(coordX)], [np.float32(coordY)]])
        self.kf.correct (measured)
        predicted = self.kf.predict()
        return predicted
#----------------------------------------------------------------------------#
#Performs required image processing to get puck coordinated in the video
class ProcessImage:
    def Setup(self):
        stepperDone = False
        print("-------------------- Initialise mode --------------------")
        time.sleep(2)
        ser.flush()
        time.sleep(1)
        ser.write("i")
        while stepperDone == False:
            table_serial_data = ser.read(ser.inWaiting())
            if "d" not in table_serial_data:
                stepperDone = False
            else:
                TABLE_WIDTH = table_serial_data[1:]
                stepperDone = True
        
        TABLE_WIDTH = int(TABLE_WIDTH)
        print(TABLE_WIDTH)
   
        # ----------- Live taking of a picture ----------------------------- # 
        #  cam = cv.VideoCapture('/home/jeffrey/tmp/met_stift.mov')
        cam = cv.VideoCapture('/dev/video1')
        cam.set(3, 1280)
        cam.set(4, 720)
        pictureTaken = False
        
        while pictureTaken == False:
            ret, frame = cam.read()
            cv.imshow("test", frame)
            if not ret:
                break
            k = cv.waitKey(1)
        
            if k%256 == 27:
                # ESC pressed
                print("Escape hit, closing...")
                break
            elif k%256 == 32:
                # SPACE pressed
                img_name = "opencv_frame_{}.png".format(1)
                cv.imwrite(img_name, frame)
                print("{} written!".format(img_name))
                pictureTaken = True
        # ----------- Getting max values of y-coords ----------------------- # 
        img = cv.imread('opencv_frame_1.png')
        # cv.imshow("image", img) 
        hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
        l_h = 0
        l_s = 0
        l_v = 131
        u_h = 180
        u_s = 255
        u_v = 243
        
        lower_red = np.array([l_h, l_s, l_v])
        upper_red = np.array([u_h, u_s, u_v])
        
        mask = cv.inRange(hsv, lower_red, upper_red)
        kernel = np.ones((5, 5), np.uint8)
        mask = cv.erode(mask, kernel)
        
        # Contours detection
        _, contours, _ = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        
        ylist = []

        for cnt in contours:
            area = cv.contourArea(cnt)
            approx = cv.approxPolyDP(cnt, 0.02*cv.arcLength(cnt, True), True)
            x = approx.ravel()[0]
            y = approx.ravel()[1]
            ylist.append(y)
                
            if area > 400:
                cv.drawContours(img, [approx], 0, (0, 0, 0), 5)
        
                # if len(approx) == 3:
                    # cv.putText(frame, "Triangle", (x, y), font, 1, (0, 0, 0))
                # if len(approx) == 4:
                    # cv.putText(img, "Rectangle", (x, y), font, 1, (0, 0, 0))
                # elif 10 < len(approx) < 20:
                    # cv.putText(frame, "Circle", (x, y), font, 1, (0, 0, 0))
        
        # cv.imshow("test", img)
        # cv.imshow("te", mask)

        print(ylist)
        max_value = max(ylist)
        min_value = min(ylist)
        max_value = max_value - 36
        min_value = min_value + 15
        cam.release()
        cv.destroyAllWindows()
        return max_value, min_value, TABLE_WIDTH

    def DetectObject(self):
        max_value, min_value, TABLE_WIDTH = self.Setup()
        print("-------------------- Predicting mode --------------------\n")
        #  Difficulties
        DIFF_HARD = 1
        DIFF_MEDIUM = 0.9
        DIFF_EASY = -1
        DIFF_DYNAMIC = 1
        actualDifficulty = DIFF_HARD
        # Goals scored
        awayGoals = 0
        homeGoals = 0
        [ppuckX, ppuckY] = [0, 0]
        malletX = 1109
        goalX = malletX+100
        

        # 1280x720
        # vid = cv.VideoCapture('/home/jeffrey/tmp/lol.mov')
        print('oke') 
        #  vid = cv.VideoCapture('/home/jeffrey/tmp/met_stift.mov')
        vid = cv.VideoCapture('/dev/video1')
        vid.set(3, 1280)
        vid.set(4, 720)

        # vid = cv.VideoCapture('out.mp4')

        if(vid.isOpened() == False):
            print('Cannot open input video')
            return

        width = int(vid.get(3))
        height = int(vid.get(4))
        print(width)
        print(height)
        
        print(max_value)
        print(min_value)
        scale = TABLE_WIDTH/(max_value - min_value)
        goalwidth = ((max_value-min_value)/2)+min_value
        print(scale)
        print(goalwidth)


        # Create Kalman Filter Object
        kfObj = KalmanFilter()
        predictedCoords = np.zeros((2, 1), np.float32)


        
        ser.write("p")

        while(vid.isOpened()):
            rc, framepuck = vid.read()

            # raw = ser.readline() 
            # serial_data = raw.strip().decode("utf-8")
        
            serial_data = ser.read(ser.inWaiting())
            
            if serial_data == "kaas":
                print("msg received")
            if serial_data == "hard":
                actualDifficulty = DIFF_HARD
                print("HARD")
            elif serial_data == "medium":
                print("MEDIUM")
                actualDifficulty = DIFF_MEDIUM
            elif serial_data == "easy":
                print("EASY")
                actualDifficulty = DIFF_EASY
            elif serial_data == "dynamic":
                print("DYNAMIC")
                actualDifficulty = DIFF_DYNAMIC
            elif serial_data == "gfa":
                print("GOAL SCORED FOR AWAY")
                awayGoals = awayGoals+1
            elif serial_data == "gfh":
                print("GOAL SCORED FOR HOME")
                homeGoals = homeGoals+1

            if actualDifficulty == DIFF_DYNAMIC:
                if((homeGoals - awayGoals) > 0):
                    actualDifficulty = DIFF_HARD
                elif((homeGoals - awayGoals) < 0):
                    actualDifficulty = DIFF_EASY
                elif((homeGoals - awayGoals) == 0):
                    actualDifficulty = DIFF_MEDIUM
                

            if(rc == True):
                [puckX, puckY] = self.DetectPuck(framepuck)
                predictedCoords = kfObj.Estimate(puckX, puckY)
            
                #  print("previous:",ppuckX)
                #  print("new:",puckX)

                # Draw Actual coords from segmentation
                cv.circle(framepuck, (int(puckX), int(puckY)), 10, [0,0,255], 2, 8)

                #  followPuck = [0]
                #  followPuck[0] = abs(scale*(puckY) - (TABLE_WIDTH)-min_value) 
                #  if followPuck[0] < (3114):
                    #  time.sleep(0.1)
                    #  followPuck = str(followPuck)
                    #  ser.write("d")
                    #  ser.write(followPuck)
                    #  print(followPuck)

                # cv.line(framepuck,(0, 170), (width, 170), [255, 0, 0], 2, 8)
                # cv.line(framepuck,(0, 20), (width, 20), [255, 0, 0], 2, 8)

                #old border lines
                # cv.line(framepuck,(0, 300), (width, 300), [255, 0, 0], 2, 8)
                # cv.line(framepuck,(0, 0), (width, 0), [255, 0, 0], 2, 8)
                
                
                circlecounter = 0
                #  xn = puckX
                #  yn = puckY
                #  xn1 = (predictedCoords[0])
                #  yn1 = (predictedCoords[1])
                #  dx = predictedCoords[0] - puckX
                #  dy = predictedCoords[1] - puckY
                xn = ppuckX
                yn = ppuckY
                xn1 = puckX
                yn1 = puckY
                dx = puckX - ppuckX
                dy = puckY - ppuckY

                #  for x in range(1000, 2000, 20): 
                    #  time.sleep(1)
                    #  Coord = [0]
                    #  Coord[0] = x
                    #  goToCoord = str(Coord)
                    #  ser.write("d")
                    #  ser.write(goToCoord)
                    #  print(goToCoord)
                
                #  for x in range(1000, 2000, 20): 
                    #  Coord = [0]
                    #  Coord[0] = x
                    #  goToCoord = str(Coord)
                    #  ser.write("d")
                    #  ser.write(goToCoord)
                    #  print(goToCoord)

                for x in range(0, 150): 
                    cv.line(framepuck,(int(xn), int(yn)), (xn1, yn1), [0, 0, 0], 2, 8)
                    xn = xn1
                    yn = yn1
                    if(dx != 0 or dy != 0):
                        lineY=(dy/dx)*xn1

                    if((dx) < 2):
                        break

                    xn1 = xn1 + dx
                    yn1 = yn1 + dy
                    if(yn1 < max_value and yn1 > min_value):
                        dy = dy
                    if(yn1 >= max_value or yn1 <= min_value):
                        dy = -dy
                    
                        
                    #  if(lineY >= goalwidth-100 and lineY <= goalwidth+100 and xn1 >= 1130 and xn1 <= 1210):
                        #  cv.circle(framepuck, (int(xn1), int(yn1)), 10, [255,0,0], 2, 8)
                        #  circlecounter = circlecounter+1
                        #  if circlecounter > 1:
                            #  print("Shoot!")

                        #  goToCoord = ((scale*(720-yn1))-(720-max_value))
                        #  goToCoord = str(goToCoord)
                        #  ser.write("d")
                        #  ser.write(goToCoord) 
                    #  if(xn1 >= 1130 and xn1 <= 1210 and yn1 >= goalwidth-100 and yn <= goalwidth+100):
                        #  cv.circle(framepuck, (int(xn1), int(yn1)), 10, [0,255,0], 2, 8)
                        #  goToCoord = scale*(yn1-min_value)
                        #  goToCoord = scale*(yn1)
                        #  goToCoord = str(goToCoord)
                        #  ser.write("d")
                        #  ser.write(goToCoord)
                    if(xn1 > malletX and xn < malletX):
                        #  getting the pucks line formula
                        dx = puckX - ppuckX
                        dy = puckY - ppuckY
                        rcp = (dy/dx)
                        b = -(rcp*ppuckX)+ppuckY
                        malletY = rcp*malletX+b
                        print("malletY",malletY)
                        
                        goToCoord = ((scale*(720-malletY))-(720-max_value))
                        goalY = rcp*goalX+b
                        if(goToCoord < 3100 and goToCoord > 10 and goalY > goalwidth-70 and goalY < goalwidth+70):
                            cv.circle(framepuck, (int(xn1), int(yn1)), 10, [255,0,0], 2, 8)
                            goToCoord = str(goToCoord)
                            ser.write("d[")
                            ser.write(goToCoord)
                            ser.write("]")
                            print(goToCoord)

                    
                    #  if(xn1 >= 1100 and xn1 <= 1230 and yn1 >= goalwidth-200 and yn <= goalwidth+200):
                        #  cv.circle(framepuck, (int(xn1), int(yn1)), 10, [255,0,0], 2, 8)
                        #  circlecounter = circlecounter+1
                        #  #  if circlecounter > 1:
                            #  #  print("Shoot!")

                        #  goToCoord = ((scale*(720-yn1))-(720-max_value))
                        #  if(goToCoord < 3100 and goToCoord > 10):
                            #  goToCoord = str(goToCoord)
                            #  ser.write("d[")
                            #  ser.write(goToCoord)
                            #  ser.write("]")
                            #  print(goToCoord)
                        
                        #  newThing = actualDifficulty * goToCoord
                        #  print(newThing)
                [ppuckX, ppuckY] = [puckX, puckY]
                cv.imshow("Input",framepuck)
                
                
                if (cv.waitKey(1) & 0xFF == ord('w')):
                    break

            else:
                break

        vid.release()
        cv.destroyAllWindows()

    # Segment the green puck in a given frame
    def DetectPuck(self, framepuck):
        # Set threshold to filter only green color & Filter it
        lowerBound = np.array([90,165,45], dtype = "uint8")
        upperBound = np.array([150,255,140], dtype = "uint8")
        greenMask = cv.inRange(framepuck, lowerBound, upperBound)

        # Dilate
        kernel = np.ones((5, 5), np.uint8)
        greenMaskDilated = cv.dilate(greenMask, kernel)
        #cv.imshow('Thresholded', greenMaskDilated)

        # Find puck blob as it is the biggest green object in the framepuck
        [nLabels, labels, stats, centroids] = cv.connectedComponentsWithStats(greenMaskDilated, 8, cv.CV_32S)

        # First biggest contour is image border always, Remove it
        stats = np.delete(stats, (0), axis = 0)
        try:
            maxBlobIdx_i, maxBlobIdx_j = np.unravel_index(stats.argmax(), stats.shape)

        # This is our puck coords that needs to be tracked
            puckX = stats[maxBlobIdx_i, 0] + (stats[maxBlobIdx_i, 2]/2)
            puckY = stats[maxBlobIdx_i, 1] + (stats[maxBlobIdx_i, 3]/2)
            return [puckX, puckY]
        except:
               pass

        return [0,0]


#Main Function
def main():
    
    processImg = ProcessImage()
    processImg.DetectObject()
    

if __name__ == "__main__":
    main()

print('Program Completed!')
