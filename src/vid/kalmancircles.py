#! /usr/bin/env python2.7

#--------------------------------------------------------------------
# Implements Ball motion prediction using Kalman Filter
#
# Author: Sriram Emarose [sriram.emarose@gmail.com]
#
#
#
#--------------------------------------------------------------------

import cv2 as cv
import sys

#Main Function
def main():
    vid = cv.VideoCapture('/dev/video0')

    if(vid.isOpened() == False):
        print('Cannot open input video')
        return

    width = int(vid.get(3))
    height = int(vid.get(4))
    print(width)
    print(height)

    # Create Kalman Filter Object
    while(vid.isOpened()):
        rc, frame = vid.read()

        if(rc == True):
            cv.imshow('Input',frame)
            if (cv.waitKey(1) & 0xFF == ord('q')):
                break

        else:
            break

    vid.release()
    cv.destroyAllWindows()

if __name__ == "__main__":
    main()

print('Program Completed!')
