\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}% 
\contentsline {section}{\numberline {2}Recognition problem}{3}{section.2}% 
\contentsline {section}{\numberline {3}Our system}{3}{section.3}% 
\contentsline {subsection}{\numberline {3.1}First tests}{4}{subsection.3.1}% 
\contentsline {subsubsection}{\numberline {3.1.1}Face detection}{4}{subsubsection.3.1.1}% 
\contentsline {subsubsection}{\numberline {3.1.2}Contrast detection}{5}{subsubsection.3.1.2}% 
