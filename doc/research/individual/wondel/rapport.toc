\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{4}{chapter.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Preliminary Investigation}{5}{chapter.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Method}{6}{chapter.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Results}{7}{chapter.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}The best way of building the robot frame}{7}{section.4.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}The Robotarm}{7}{section.4.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}VEX}{7}{subsection.4.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Wire-drive Mechanism}{8}{subsection.4.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}The Slider Model}{9}{section.4.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Conclusion}{11}{chapter.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliography}{12}{chapter*.7}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Appendix}{14}{chapter*.8}% 
