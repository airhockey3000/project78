#include <U8g2lib.h>
#include <U8x8lib.h>

/*
 * Copyright 2019, Jeffrey Stoel,
 *	  	   Quentin Hoogwerf,
 *		   Paul Wondel,
 *		   Paul de Hek,
 *		   AirHockey3000, All rights reserved.
 */

/* Including libraries */
#include <LiquidCrystal.h>
#include <SPI.h>
#include <Wire.h>
#include <AccelStepper.h>

/* General defines */
#define DEBUG 			0

/*
---- pin definitions from: https://reprap.org/wiki/RAMPS_1.4#Firmware_and_Pin_Assignments
*/
#define X_STEP_PIN      	54
#define X_DIR_PIN       	55
#define X_ENABLE_PIN    	38
#define X_MIN_PIN       	3       //endstop
#define X_MAX_PIN       	2       //endstop

#define Y_STEP_PIN         	60
#define Y_DIR_PIN          	61
#define Y_ENABLE_PIN       	56


#define Y_MIN_PIN          	14 //home/robot goal
#define Y_MAX_PIN          	15 //away/player goal
#define Y_ENABLE_PIN    	56

#define HOME_GOAL       	18 //Y_MIN_PIN: home/robot
#define AWAY_GOAL       	19 //Y_MAX_PIN: away/player

#define MAX_SPEED 		3000 //3500	//Maximum speed the stepper motor runs.
#define ACCELERATION 		5000
#define HOME_SPEED 		256		//The speed when homing. Lower cause more accurate.

/* Buttons */
#define diffButton 		99//13		//Button for setting difficulty of the robot.

#define BTN_ENC 35  //draaiknop indrukken
#define BEEPER 37   //buzzer
#define BTN_EN2 31  //draaiknop linksom
#define BTN_EN1 33  //draaiknop rechtsom

/* Difficulties of the robot */
#define DIFF_EASY 		0.8
#define DIFF_MEDIUM 		0.9
#define DIFF_HARD 		1
#define DIFF_DYNAMIC 		1

#define FAN_PIN                 8
/*
 * Defs for the serial protocol, when the incoming char is a [ we know it's a
 * coordinate, therefore we use a char array. So we can get the full coordinate.
 */
const byte numChars = 		32;
char receivedChars[numChars];
boolean recvCoordinates = 	false;
boolean newData = 		false;
char serialCommand;
String yCoordinates;
int oldS = 			0;
boolean run = 			false;


/* Fan state */
boolean fanEnabled = false;

/* Max position of the stepper, predefined as 1300 */
boolean maxPosSet = 		false;
int MAX_POS = 			3000;

/* Scoreboard pins and defs */
//const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
const int rs = 99, en = 98, d4 = 97, d5 = 96, d6 = 95, d7 = 94;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);


//full graphic
//U8G2_ST7920_128X64_1_SW_SPI u8g2(U8G2_R0, /* clock=*/ 23 /* A4 */ , /* data=*/ 17 /* A2 */, /* CS=*/ 16 /* A3 */, /* reset=*/ U8X8_PIN_NONE);

U8G2_ST7920_128X64_F_SW_SPI u8g2(U8G2_R0, /* clock=*/ 23 /* A4 */ , /* data=*/ 17 /* A2 */, /* CS=*/ 16 /* A3 */, /* reset=*/ U8X8_PIN_NONE);

int homeG = 			0;
int awayG = 			0;

char chomeG [5];
char cawayG [5];

volatile boolean homeGoalTriggered = false;
volatile boolean awayGoalTriggered = false;

/* States */
int buttonState =	 	0;

/* Current difficulty */
int diffLevel = 		2;              //2 HARD 1 MEDIUM 0 EASY 3 DYNAMIC

/* Create stepper object */
AccelStepper stepper(1, X_STEP_PIN, X_DIR_PIN); //motor interface type must be set to 1 when using a driver.
AccelStepper stepperY(1, Y_STEP_PIN, Y_DIR_PIN); //motor interface type must be set to 1 when using a driver.



/* Home function, sets the stepper to the home position */
void home()
{
        stepper.setMaxSpeed(HOME_SPEED);
        if (DEBUG) {
                Serial.println("---Homing...---");
        }
        while (digitalRead(X_MIN_PIN) != LOW) {
                if (DEBUG) {
                        //Serial.println("Home not end triggered");
                }
                stepper.move(-10);
                stepper.run();
        }
        stepper.setCurrentPosition(0);
        stepper.runToNewPosition(10);
        if (DEBUG) {
                Serial.println("Position set to zero");
                Serial.print("Current position is:");
                Serial.println(stepper.currentPosition());
        }
        stepper.setMaxSpeed(MAX_SPEED);
        serialCommand = '0';
}

/* Function that calculates max steps stepper can make */
void setMaxPosition()
{
        if (DEBUG) {
                Serial.println("---Calculating max position---");
        }
        home();
        delay(500);
        stepper.setMaxSpeed((int)(MAX_SPEED / 3));
        while (digitalRead(X_MAX_PIN) != LOW) {
                if (DEBUG) {
                        //Serial.println("Max not end triggered");
                }
                stepper.move(10);
                stepper.run();
        }
        if (DEBUG) {
                Serial.println("Max end triggered");
        }
        MAX_POS = (stepper.currentPosition() - 10);
        stepper.runToNewPosition(MAX_POS - 10);
        if (DEBUG) {
                Serial.print("New max position: ");
                Serial.println(MAX_POS);
        }
        maxPosSet = true;
        serialCommand = '0';
        stepper.setMaxSpeed(MAX_SPEED);
}

/* Function that moves the stepper to a absolute position */
void moveTo(int position) {
        if (position >= 0 || position <= MAX_POS){
                stepper.moveTo(position);
        }
        else{
		stepper.move(0);
        }
        return;
}

//move naar relatieve postitie: dus move(10) is +10 stappen vanaf huidige postitie
// move(-10) -10 stappen vanaf huidige positie
void move(int relativePosition) {
        int newPosition = (stepper.currentPosition() + relativePosition);
        moveTo(newPosition);
}

//move naar positie in while loop, dus blocking!!!
void runStepperPos() {
        while(stepper.currentPosition() != stepper.targetPosition()){
                //stepper.runToPosition();
                stepper.run();
        }
        if (DEBUG) {
                if (stepper.currentPosition() == stepper.targetPosition()) {
                        Serial.print("Moved to ");
                        Serial.println(stepper.currentPosition());
                }
        }
}

void stepperControl(boolean state){
	stepper.enableOutputs();
}

/* lcdSetup function, home screen kind of look */
void lcdSetup() {
	// set up the LCD's number of columns and rows:
	lcd.begin(16, 2);
	// Print a message to the LCD.
	lcd.setCursor(2,0);
	lcd.print("Home -- Away");
	lcd.setCursor(2,1);
	lcd.print("Game  Scores");
}

void u8g2Setup(){
        u8g2.setFont(u8g2_font_6x10_tf);
        u8g2.setFontRefHeightExtendedText();
        u8g2.setDrawColor(1);
        u8g2.setFontPosTop();
        u8g2.setFontDirection(0);
}

/* Case switch for serial input */
void serialFunction(char input){
        //if(digitalRead(homeButton)==HIGH){
        //else if(digitalRead(awayButton)==HIGH){
        switch (input) {
        /* Reset */
        case 'r':
                homeG=0;
                awayG=0;
                resetScreen(2,0);
                lcd.print("Home -- Away");
                lcd.setCursor(3,1);
                lcd.print(homeG);
                lcd.setCursor(12,1);
                lcd.print(awayG);
                break;
        /* Initialize */
        case 'i':
                u8g2.setFont(u8g2_font_ncenB10_tr);
                u8g2.clearBuffer();
                u8g2.drawStr(0,20,"Fan test");
                u8g2.sendBuffer();
                fanControl(true);
                delay(1000);
                fanControl(false);
                stepperControl(true);
                //stepper.enableOutputs();
                resetScreen(0,0);
		chooseDifficulty();
                //lcd.print("Initialising.");

                u8g2.clearBuffer();
                u8g2.setFont(u8g2_font_ncenB10_tr);
                u8g2.drawStr(0,20,"Initializing");
                u8g2.sendBuffer();
		setMaxPosition();

                resetScreen(0,0);
                lcd.print("Initialising..");
                delay(500);
                resetScreen(0,0);
		moveTo((int)(MAX_POS/2));
		stepper.runToPosition();
                serialCommand = '0';
                lcd.print("Initialising...");
		String maxPos = String(MAX_POS);
		Serial.print("d"+maxPos);
                delay(500);
                resetScreen(0,0);
                lcd.print("Press space if all");
                lcd.setCursor(0,1);
                lcd.print("demands are correct");

                u8g2.clearBuffer();
                u8g2.setFont(u8g2_font_ncenB10_tr);
                u8g2.drawStr(0,0,"Press space if");
                u8g2.drawStr(0,15,"all demands are");
                u8g2.drawStr(0,30,"correct.");
                u8g2.sendBuffer();

                delay(500);
		gameStart();
                break;
        /* Prediction mode */
        case 'p':
                resetScreen(1,0);
                lcd.print("You're never scoring!");
                delay(500);

                break;
        case 's':
                resetScreen(1,0);
              	shoot();	
                u8g2.clearBuffer();
                u8g2.setFont(u8g2_font_ncenB10_tr);
                u8g2.drawStr(0,0,"Shoot");
                u8g2.sendBuffer();
                delay(500);
                break;
        /* Goal for home */
        /******** zie goal handler in loop(); *******
        case 'H':
                homeG++;
                resetScreen(2,0);
                lcd.print("Home -- Away");
                lcd.setCursor(3,1);
                lcd.print(homeG);
                lcd.setCursor(12,1);
                lcd.print(awayG);
                break;
        */

        /* Goal for away */
        /******** zie goal handler in loop(); ******
        case 'A':
                awayG++;
                resetScreen(2,0);
                lcd.print("Home -- Away");
                lcd.setCursor(3,1);
                lcd.print(homeG);
                lcd.setCursor(12,1);
                lcd.print(awayG);
                break;
        */
        //test, mag straks weggehaald worden
        case '5':
                setMaxPosition();
                Serial.println("Test complete!");
                break;
        //einde test
        default:
                //stepper.disableOutputs();
                stepperControl(false);
                fanControl(false);
                resetScreen(0, 0);
                lcd.print("SOMETHING WENT WRONG");
                lcd.setCursor(0,1);
                lcd.print(input);
                Serial.print(input);
                break;
        }
}

/* Resets the screen */
void resetScreen(int y, int x){
  lcd.clear(); // this is to clear the screen of the previous displayed text
  lcd.setCursor(y, x); // set the cursor to column 0, line 0 or 1
                      // (note: line 1 is the second row, since counting begins with 0):
}

/* Difficulty changing function */
void difffunc() {
    diffLevel--;
    delay(100);
    if(diffLevel < 0) {
        diffLevel = 3;
    }
    if(diffLevel == 3) {
        resetScreen(2,0);
        lcd.print("Difficulty");
        lcd.setCursor(3,1);
        lcd.print("D Y N A M I C");
        Serial.print("dynamic");
    }else if(diffLevel == 2) {
        resetScreen(2,0);
        lcd.print("Difficulty");
        lcd.setCursor(3,1);
        lcd.print("H A R D");
        Serial.print("hard");
    } else if(diffLevel == 1) {
        resetScreen(2,0);
        lcd.print("Difficulty");
        lcd.setCursor(3,1);
        lcd.print("M E D I U M");
       Serial.print("medium");
    } else if(diffLevel == 0) {
        resetScreen(2,0);
        lcd.print("Difficulty");
        lcd.setCursor(3,1);
        lcd.print("E A S Y");
        Serial.print("easy");
    }
}

/* Function when receiving a coordinate */
void recvString() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '[';
    char endMarker = ']';
    char rc;

    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();
	stepper.run();

        if (recvInProgress == true) {
	    stepper.run();
            if (rc != endMarker) {
		stepper.run();
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
		    stepper.run();
                    ndx = numChars - 1;
                }
		if (rc == 'd') {
			recvInProgress = false;
			newData = false;
                	receivedChars[ndx] = '\0'; // terminate the string
			ndx = 0;
		}
		if (rc == 's') {
			shoot();
                	u8g2.clearBuffer();
                	u8g2.setFont(u8g2_font_ncenB10_tr);
                	u8g2.drawStr(0,0,"Shoot");
                	u8g2.sendBuffer();
		}
            }
            else {
		stepper.run();
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
	    stepper.run();
            recvInProgress = true;
        }
    }
}

/* Function to show the data received */
void showNewData() {
    if (newData == true) {
    	String str(receivedChars);
	int curPos = stepper.currentPosition();
	/* String curPosS = String(curPos, DEC); */
        /* Serial.print("This just in ... "); */
	while(true) {
		run = stepper.isRunning();
		if(run < 1){
        		//u8g2.clearBuffer();
        		//u8g2.drawStr(0,20,receivedChars);
        		/* u8g2.drawstr(0,25,curposs); */
        		//u8g2.sendBuffer();

        		/* Serial.println(str.toInt()); */
        		newData = false;
			//moveTo(str.toInt());
        		//u8g2.drawStr(0,40,"kaas");
        		/* u8g2.drawstr(0,25,curposs); */
        		//u8g2.sendBuffer();
			stepper.run();
			break;

		}
		stepper.run();

	}
    }
}

void homeGoal_ISR(){
        homeGoalTriggered = true;
}

void awayGoal_ISR(){
        awayGoalTriggered = true;
}

void fanControl(boolean state){
        fanEnabled = state;
        digitalWrite(FAN_PIN, fanEnabled);
}

void gameOver(){
        detachInterrupt(digitalPinToInterrupt(HOME_GOAL));
        detachInterrupt(digitalPinToInterrupt(AWAY_GOAL));

        fanControl(false);
        stepperControl(false);

}

void gameStart(){

        homeG = 0;
        awayG = 0;

        attachInterrupt(digitalPinToInterrupt(HOME_GOAL), homeGoal_ISR, FALLING);
        attachInterrupt(digitalPinToInterrupt(AWAY_GOAL), awayGoal_ISR, FALLING);

        fanControl(true);

        stepperControl(true);
}

void shoot(){
        stepperY.move(200);
}

/* Setup function */
void setup()
{
        Serial.begin(115200);
	u8g2.begin();
	/* Scoreboard stuff */
	pinMode(diffButton, INPUT_PULLUP);
	lcdSetup();

	/* Stepper setup */
        pinMode(X_MIN_PIN, INPUT_PULLUP);
        pinMode(X_MAX_PIN, INPUT_PULLUP);
        // pinMode(HOME_GOAL, INPUT_PULLUP);
        // pinMode(AWAY_GOAL, INPUT_PULLUP);
        pinMode(X_ENABLE_PIN, OUTPUT);
        pinMode(Y_ENABLE_PIN, OUTPUT);

        stepper.setMaxSpeed(MAX_SPEED); //maximum steps per second
        stepper.setAcceleration(ACCELERATION);
        stepper.setEnablePin(X_ENABLE_PIN);
        stepper.setPinsInverted(0,0,1); //direction invert, step invert, enable invert
	stepper.setMinPulseWidth(1);

        u8g2Setup();
        u8g2.setFont(u8g2_font_7x13_tf);
        u8g2.firstPage();
        do {
                u8g2.drawStr(0, 0, "Setup complete!");
        } while( u8g2.nextPage() );
		
	u8g2.clearBuffer();
	sprintf(chomeG, "%d", homeG);
	sprintf(cawayG, "%d", awayG);
	u8g2.drawStr(0, 0, "HOME -- AWAY");
	u8g2.drawStr(0, 20, chomeG);
	u8g2.drawStr(20, 20, cawayG);
	u8g2.sendBuffer();

        if (DEBUG) {
                Serial.println("Setup complete \n");
        }
	/* Serial.println("h: Home\nm: set min/max position\nc: center\nd: demo\ni: interactive demo\n\nEnter command:"); */


}

/* Loop function */
void loop()
{

        /*TODO */
        //gameStart();

        //gameOver();

        //als hij het niet doe, regel hieronder weghalen
        stepper.run();
        stepperY.run();

	buttonState = digitalRead(diffButton);
	/* Read Serial, if the serial is d then we are receiving coordinates, else see switch case */
        if (Serial.available()) {
                serialCommand = Serial.read();
                /*coordinate handler*/
		if(serialCommand != 'd') {
			recvCoordinates = false;
			serialFunction(serialCommand);

		} else {
			recvCoordinates = true;
			while(recvCoordinates == true) {
				stepper.run();
                                stepperY.run();
				recvString();
				showNewData();
			}
		}
        }

        /* Goal handler */
        if (homeGoalTriggered || awayGoalTriggered){    //als goal is gemaakt
                if(homeGoalTriggered){
                        homeG++;                        //verhoog home score
                }
                if(awayGoalTriggered){
                        awayG++;                        //verhoog away score
                }
                homeGoalTriggered = false;              //reset ISR flag
                awayGoalTriggered = false;              //reset ISR flag
		u8g2.clearBuffer();
		sprintf(chomeG, "%d", homeG);
		sprintf(cawayG, "%d", awayG);
		u8g2.drawStr(0, 0, "HOME -- AWAY");
		u8g2.drawStr(0, 20, chomeG);
		u8g2.drawStr(20, 20, cawayG);
		u8g2.sendBuffer();
        }
}


void chooseDifficulty() {
	u8g2.clearBuffer();
	u8g2.setDrawColor(2);
	u8g2.drawStr(5, 10, "EASY");
	u8g2.drawStr(5, 20, "MEDIUM");
	u8g2.setFontMode(1);
	u8g2.drawStr(5, 30, "HARD");
	u8g2.setDrawColor(0);
	u8g2.drawStr(5, 40, "DYNAMIC");
	u8g2.setFontMode(1);
	while(true) {

		if(digitalRead(BTN_ENC) == HIGH) {
			break;	
		}
	}
}
