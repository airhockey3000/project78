#! /usr/bin/env python2.7
#----------------------------------------------------------------------------#
#  Imports
import serial
import cv2 as cv
import numpy as np
import sys
import os
import time
#----------------------------------------------------------------------------#
#Setup serial
#ttyUSB0 is configured for a linux machinser = serial.Serial('/dev/ttyUSB0',9600, timeout=0.001) 
ser = serial.Serial('/dev/ttyACM0',115200, timeout=0.001) 
#----------------------------------------------------------------------------#

# ProcessImage Performs required image processing to get puck coordinated in the video
# Setup goed into a initialise mode where we get the amount of steps the table is
# 
# First the python code sends a message that its waiting for the initialize.
# Then the code waits for the reaction of the arduino code which sends the steps of the table.
#
# Figure underneath is the airhockeytable where x is the position of the steppers arm.
# 3180 is the estimate we are getting, would work on any table width if used properly.
#
#       ________________________________  3180
#       |                            x  |
#       |                            x  | 
#       |                            x  | motor
#       |                            x  | steps
#       |                            x  |
#       |                            x  |
#       ________________________________  0
#       
# When the counting of steps is done, the robot centers. The computer vision then waits
# for the players to press space to count the tables width in screen coordinates.
# 
# This is done by taking a picture of the camera and converting the image to black
# and white, where the contours are spotted easily

class ProcessImage:
    def Setup(self):
        stepperDone = False
        print("-------------------- Initialise mode --------------------")
        time.sleep(2)
        ser.flush()
        time.sleep(1)
        ser.write("i")
        while stepperDone == False:
            table_serial_data = ser.read(ser.inWaiting())
            if "d" not in table_serial_data:
                stepperDone = False
            else:
                TABLE_WIDTH = table_serial_data[1:]
                stepperDone = True
        
        TABLE_WIDTH = float(TABLE_WIDTH)
        print(TABLE_WIDTH)
   
        # ----------- Live taking of a picture ----------------------------- # 
        cam = cv.VideoCapture('/dev/video2')
        cam.set(3, 1280)
        cam.set(4, 720)
        pictureTaken = False
        
        while pictureTaken == False:
            ret, frame = cam.read()
            cv.imshow("test", frame)
            if not ret:
                break
            k = cv.waitKey(1)
        
            if k%256 == 27:
                # ESC pressed
                print("Escape hit, closing...")
                break
            elif k%256 == 32:
                # SPACE pressed
                img_name = "opencv_frame_{}.png".format(1)
                cv.imwrite(img_name, frame)
                print("{} written!".format(img_name))
                pictureTaken = True
        # ----------- Getting max values of y-coords ----------------------- # 
        img = cv.imread('opencv_frame_1.png')
        hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
        l_h = 0
        l_s = 0
        l_v = 131
        u_h = 180
        u_s = 255
        u_v = 243
        lower_red = np.array([l_h, l_s, l_v])
        upper_red = np.array([u_h, u_s, u_v])
        mask = cv.inRange(hsv, lower_red, upper_red)
        kernel = np.ones((5, 5), np.uint8)
        mask = cv.erode(mask, kernel)
        # Contours detection
        contours, hierarchy = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        
        ylist = []

        for cnt in contours:
            area = cv.contourArea(cnt)
            approx = cv.approxPolyDP(cnt, 0.02*cv.arcLength(cnt, True), True)
            x = approx.ravel()[0]
            y = approx.ravel()[1]
            ylist.append(y)
                
            if area > 400:
                cv.drawContours(img, [approx], 0, (0, 0, 0), 5)

        print(ylist)
        max_value = max(ylist)
        min_value = min(ylist)
        max_value = max_value - 36
        min_value = min_value + 15
        cam.release()
        cv.destroyAllWindows()
        return max_value, min_value, TABLE_WIDTH

    def DetectObject(self):
        max_value, min_value, TABLE_WIDTH = self.Setup()
        print("-------------------- Predicting mode --------------------\n")
        #  Difficulties
        DIFF_HARD = 1
        DIFF_MEDIUM = 0.9
        DIFF_EASY = -1
        DIFF_DYNAMIC = 1
        actualDifficulty = DIFF_HARD
        # Goals scored
        awayGoals = 0
        homeGoals = 0
        [ppuckX, ppuckY] = [0, 0]
        malletX = 1090
        goalX = malletX+100
        

        # 1280x720
        vid = cv.VideoCapture('/dev/video2')
        vid.set(3, 1280)
        vid.set(4, 720)

        if(vid.isOpened() == False):
            print('Cannot open input video')
            return

        width = int(vid.get(3))
        height = int(vid.get(4))
        print(width)
        print(height)
        
        print(max_value)
        print(min_value)
        scale = TABLE_WIDTH/(max_value - min_value)
        goalwidth = ((max_value-min_value)/2)+min_value
        print(scale)
        print(goalwidth)

        ser.write("p")

        while(vid.isOpened()):
            rc, framepuck = vid.read()

            # Reading serial for changing difficulty LIVE
            #  serial_data = ser.read(ser.inWaiting())
            #  if serial_data == "kaas":
                #  print("msg received")
            #  if serial_data == "hard":
                #  actualDifficulty = DIFF_HARD
                #  print("HARD")
            #  elif serial_data == "medium":
                #  print("MEDIUM")
                #  actualDifficulty = DIFF_MEDIUM
            #  elif serial_data == "easy":
                #  print("EASY")
                #  actualDifficulty = DIFF_EASY
            #  elif serial_data == "dynamic":
                #  print("DYNAMIC")
                #  actualDifficulty = DIFF_DYNAMIC
            #  elif serial_data == "gfa":
                #  print("GOAL SCORED FOR AWAY")
                #  awayGoals = awayGoals+1
            #  elif serial_data == "gfh":
                #  print("GOAL SCORED FOR HOME")
                #  homeGoals = homeGoals+1

            #  if actualDifficulty == DIFF_DYNAMIC:
                #  if((homeGoals - awayGoals) > 0):
                    #  actualDifficulty = DIFF_HARD
                #  elif((homeGoals - awayGoals) < 0):
                    #  actualDifficulty = DIFF_EASY
                #  elif((homeGoals - awayGoals) == 0):
                    #  actualDifficulty = DIFF_MEDIUM
                

            if(rc == True):
                [puckX, puckY] = self.DetectPuck(framepuck)

                # Draw Actual coords from segmentation
                cv.circle(framepuck, (int(puckX), int(puckY)), 10, [0,0,255], 2, 8) 
                circlecounter = 0

                xn = ppuckX
                yn = ppuckY
                xn1 = puckX
                yn1 = puckY
                dx = puckX - ppuckX
                dy = puckY - ppuckY
                cv.line(framepuck,(int(xn), int(yn1)), (int(xn1), int(yn1)), [0, 0, 0], 2, 8)

                if(dx < 0):
                    goToCoord = str(TABLE_WIDTH/2)
                    ser.write("d[")
                    ser.write(goToCoord)
                    ser.write("]") 

                for x in range(0, 20): 
                    xn = xn1
                    yn = yn1
                    if(dx != 0 or dy != 0):
                        lineY=(dy/dx)*xn1
                    xn1 = xn1 + dx
                    yn1 = yn1 + dy
                    if(yn1 < max_value and yn1 > min_value):
                        dy = dy
                    if(yn1 >= max_value):
                        dy = -dy
                        yn1 = yn1+(yn-max_value)
                    if(yn1 <= min_value):
                        dy = -dy
                        yn1 = yn1+(yn-min_value)
                    cv.line(framepuck,(int(xn), int(yn)), (xn1, yn1), [0, 0, 0], 2, 8)

                    # If x coordinate of puck travels over robots x
                    if(xn1 > malletX and xn < malletX):
                        #  getting the pucks line formula
                        pdx = puckX - ppuckX
                        pdy = puckY - ppuckY
                        rcp = (pdy/pdx)
                        b = -(rcp*xn)+yn
                        malletY = rcp*malletX+b
                        print("malletY",malletY)
                        goToCoord = (scale*((720-malletY)-(720-max_value)))
                        goalY = rcp*goalX+b
                        #If puck intersects with the robot and trajectory is focussing goal
                        if(goToCoord < 3100 and goToCoord > 10 and goalY > goalwidth-110 and goalY < goalwidth+110):
                            cv.circle(framepuck, (int(malletX), int(malletY)), 10, [255,0,0], 2, 8)
                            goToCoord = str(goToCoord)
                            ser.write("d[")
                            ser.write(goToCoord)
                            ser.write("]")
                            print(goToCoord)
                    
                    if(puckX >= malletX):
                        ser.write("s")
                        print('shoot!') 

                [ppuckX, ppuckY] = [puckX, puckY]
                cv.imshow("Input",framepuck)
                
                if (cv.waitKey(1) & 0xFF == ord('w')):
                    break

            else:
                break

        vid.release()
        cv.destroyAllWindows()

    # Segment the green puck in a given frame
    def DetectPuck(self, framepuck):
        # Set threshold to filter only green color & Filter it
        lowerBound = np.array([0,82,0], dtype = "uint8")
        upperBound = np.array([61,255,68], dtype = "uint8")
        greenMask = cv.inRange(framepuck, lowerBound, upperBound)

        # Dilate
        kernel = np.ones((5, 5), np.uint8)
        greenMaskDilated = cv.dilate(greenMask, kernel)
        #cv.imshow('Thresholded', greenMaskDilated)

        # Find puck blob as it is the biggest green object in the framepuck
        [nLabels, labels, stats, centroids] = cv.connectedComponentsWithStats(greenMaskDilated, 8, cv.CV_32S)

        # First biggest contour is image border always, Remove it
        stats = np.delete(stats, (0), axis = 0)
        try:
            maxBlobIdx_i, maxBlobIdx_j = np.unravel_index(stats.argmax(), stats.shape)

        # This is our puck coords that needs to be tracked
            puckX = stats[maxBlobIdx_i, 0] + (stats[maxBlobIdx_i, 2]/2)
            puckY = stats[maxBlobIdx_i, 1] + (stats[maxBlobIdx_i, 3]/2)
            return [puckX, puckY]
        except:
               pass

        return [0,0]


#Main Function
def main():
    
    processImg = ProcessImage()
    processImg.DetectObject()
    

if __name__ == "__main__":
    main()

print('Program Completed!')
